Structure
=========

PDF _master\_thesis_ contains:

- documentation for the turbulence model
- PDE (partial differential equations) describing model
- introduction into spectral/hp element method
- introduction into time integration scheme used in entire Nektar++ framwork
- results of numerical tests and experiments

Folder _tests_ contains examples of input files for the turbulence model.

Source code of the model itself is embedded into Nektar++ structure.
Most important classes are _VarVisSimple_ and _Wilcox2006_. 

