///////////////////////////////////////////////////////////////////////////////
//
// File VarVisSimple.h
//
// For more information, please see: http://www.nektar.info
//
// The MIT License
//
// Copyright (c) 2006 Division of Applied Mathematics, Brown University (USA),
// Department of Aeronautics, Imperial College London (UK), and Scientific
// Computing and Imaging Institute, University of Utah (USA).
//
// License for the specific language governing rights and limitations under
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//
// Description: Simple variable viscosity model header.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef NEKTAR_SOLVERS_VARVISSIMPLE_H
#define NEKTAR_SOLVERS_VARVISSIMPLE_H

#include <LibUtilities/BasicUtils/ArrayDebugger.h>
#include <IncNavierStokesSolver/EquationSystems/IncNavierStokes.h>
#include <IncNavierStokesSolver/TurbulenceModels/Wilcox2006.h>

namespace Nektar
{
    class VarVisSimple: public IncNavierStokes
    {
    public:

        /// Creates an instance of this class
        static SolverUtils::EquationSystemSharedPtr create(
            const LibUtilities::SessionReaderSharedPtr& pSession,
            const SpatialDomains::MeshGraphSharedPtr &pGraph)
        {
            SolverUtils::EquationSystemSharedPtr p =
                MemoryManager<VarVisSimple>::AllocateSharedPtr(
                    pSession, pGraph);
            p->InitObject();
            return p;
        }

        /// Name of class
        static std::string className;

        /// Constructor.
        VarVisSimple(
            const LibUtilities::SessionReaderSharedPtr& pSession,
            const SpatialDomains::MeshGraphSharedPtr &pGraph);

        virtual ~VarVisSimple();

        virtual void v_InitObject();
        
        void SetUpPressureForcing(
                    const Array<OneD, const Array<OneD, NekDouble> > &fields,
                    Array<OneD, Array<OneD, NekDouble> > &Forcing,
                    const NekDouble aii_Dt)
        {
            v_SetUpPressureForcing( fields, Forcing, aii_Dt);
        }

        void SetUpViscousForcing(
                    const Array<OneD, const Array<OneD, NekDouble> > &inarray,
                    Array<OneD, Array<OneD, NekDouble> > &Forcing,
                    const NekDouble aii_Dt,
                    const NekDouble time)
        {
            v_SetUpViscousForcing( inarray, Forcing, aii_Dt, time);
        }
        
        void SolvePressure( const Array<OneD, NekDouble>  &Forcing)
        {
            v_SolvePressure( Forcing);
        }
        
        void SolveViscous(
                    const Array<OneD, const Array<OneD, NekDouble> > &Forcing,
                    Array<OneD, Array<OneD, NekDouble> > &outarray,
                    const NekDouble aii_Dt)
        {
            v_SolveViscous( Forcing, outarray, aii_Dt);
        }

        void SolveUnsteadyStokesSystem(
                    const Array<OneD, const Array<OneD, NekDouble> > &inarray,
                    Array<OneD, Array<OneD, NekDouble> > &outarray,
                    const NekDouble time,
                    const NekDouble a_iixDt);

        void EvaluateAdvection_SetPressureBCs(
                    const Array<OneD, const Array<OneD, NekDouble> > &inarray,
                    Array<OneD, Array<OneD, NekDouble> > &outarray,
                    const NekDouble time)
        {
            v_EvaluateAdvection_SetPressureBCs( inarray, outarray, time);
        }

        NekDouble EvaluateL2Error(int field_number,
                                  const std::string& func_name,
                                  Array<OneD, NekDouble> array_to_compare,
                                  NekDouble time);

    protected:
        /// Variable implicit viscosity used in Laplacian during solving viscous part of scheme
        StdRegions::VarCoeffMap m_varVisMap;
        /// Phys values of implicit viscosity
        Array<OneD, NekDouble> m_imp_kinVis;
        /// Phys values of explicit viscosity
        Array<OneD, NekDouble> m_exp_kinVis;
        /// Turbulent model object
        Wilcox2006 m_Wilcox2006;



        /// bool to identify if spectral vanishing viscosity is active.
        bool m_useHomo1DSpecVanVisc;
        /// bool to identify if spectral vanishing viscosity is active.
        bool m_useSpecVanVisc;
        /// cutt off ratio from which to start decayhing modes
        NekDouble m_sVVCutoffRatio;
        /// Diffusion coefficient of SVV modes
        NekDouble m_sVVDiffCoeff;
        NekDouble m_sVVCutoffRatioHomo1D;
        /// Diffusion coefficient of SVV modes in homogeneous 1D Direction
        NekDouble m_sVVDiffCoeffHomo1D;
        /// Array of coefficient if power kernel is used in SVV
        Array<OneD, NekDouble> m_svvVarDiffCoeff;
        /// Identifier for Power Kernel otherwise DG kernel
        bool m_IsSVVPowerKernel;
        /// Diffusion coefficients (will be kinvis for velocities)
        Array<OneD, NekDouble> m_diffCoeff;

        /// Desired volumetric flowrate
        NekDouble m_flowrate;
        /// Area of the boundary through which we are measuring the flowrate
        NekDouble m_flowrateArea;
        /// Flux of the Stokes function solution
        NekDouble m_greenFlux;
        /// Current flowrate correction
        NekDouble m_alpha;
        /// Boundary ID of the flowrate reference surface
        int m_flowrateBndID;
        /// Flowrate reference surface
        MultiRegions::ExpListSharedPtr m_flowrateBnd;
        /// Stokes solution used to impose flowrate
        Array<OneD, Array<OneD, NekDouble> > m_flowrateStokes;
        /// Output stream to record flowrate
        std::ofstream m_flowrateStream;
        /// Interval at which to record flowrate data
        int m_flowrateSteps;
        /// Value of aii_dt used to compute Stokes flowrate solution.
        NekDouble m_flowrateAiidt;

        void SetupFlowrate(NekDouble aii_dt);
        NekDouble MeasureFlowrate(
            const Array<OneD, Array<OneD, NekDouble> > &inarray);

        // Virtual functions
        virtual bool v_PostIntegrate(int step);

        void v_ExtraFldOutput(
            std::vector<Array<OneD, NekDouble> > &fieldcoeffs,
            std::vector<std::string>             &variables);

        virtual void v_GenerateSummary(SolverUtils::SummaryList& s);

        virtual void v_TransCoeffToPhys(void);

        virtual void v_TransPhysToCoeff(void);

        virtual void v_DoInitialise(void);

        virtual Array<OneD, bool> v_GetSystemSingularChecks();

        virtual int v_GetForceDimension();
        
        virtual void v_SetUpPressureForcing(
                    const Array<OneD, const Array<OneD, NekDouble> > &fields,
                    Array<OneD, Array<OneD, NekDouble> > &Forcing,
                    const NekDouble aii_Dt);
        
        virtual void v_SetUpViscousForcing(
                    const Array<OneD, const Array<OneD, NekDouble> > &inarray,
                    Array<OneD, Array<OneD, NekDouble> > &Forcing,
                    const NekDouble aii_Dt,
                    const NekDouble time);
        
        virtual void v_SolvePressure( const Array<OneD, NekDouble>  &Forcing);

        virtual void v_SolveViscous(const Array<OneD, const Array<OneD, NekDouble> > &Forcing,
                    Array<OneD, Array<OneD, NekDouble> > &outarray,
                    const NekDouble aii_Dt);
        
        virtual void v_EvaluateAdvection_SetPressureBCs(
                    const Array<OneD, const Array<OneD, NekDouble> > &inarray,
                    Array<OneD, Array<OneD, NekDouble> > &outarray,
                    const NekDouble time);

        virtual bool v_RequireFwdTrans()
        {
            return false || m_flowrate > 0.0;
        }

        virtual std::string v_GetExtrapolateStr(void)
        {
            return "Standard";
        }
        
        virtual std::string v_GetSubSteppingExtrapolateStr(
                                               const std::string &instr)
        {
            return instr;
        }
        
        Array<OneD, Array< OneD, NekDouble> > m_F;

        void SetUpSVV(void);
        void SetUpExtrapolation(void);
        
        void SVVVarDiffCoeff(const NekDouble velmag, 
                             Array<OneD, NekDouble> &diffcoeff,
                             const Array<OneD, Array<OneD, NekDouble> >
                             &vel = NullNekDoubleArrayofArray);
        void AppendSVVFactors(
                              StdRegions::ConstFactorMap &factors,
                              MultiRegions::VarFactorsMap &varFactorsMap);
    private:
        /// Computes phys values of following expression in 2D
        /// Adds computed value to outarray.
        /// outarray += [grad(velocity)^T](grad(viscosity))
        void add_GradVelTrans_Times_GradVis(const Array<OneD, const Array<OneD, NekDouble> > &velocity,
                                                const Array<OneD, NekDouble> &viscosity,
                                                Array<OneD, Array<OneD, NekDouble> > &outarray);

        /// Computes phys values of following expression in 2D
        /// Adds computed value to outarray.
        /// outarray += div[ exp_kinVis*[grad(velocity) + grad(velocity)^T] ]
        void add_ExplicitDiffusion(const Array<OneD, const Array<OneD, NekDouble> > &inarray,
                                                const Array<OneD, NekDouble> &exp_kinVis,
                                                Array<OneD, Array<OneD, NekDouble> > &outarray);

        /// Splits innaray of total_kinVis into two components - implicit and explicit viscosity.
        /// Updates values of m_imp_kinVis and m_exp_kinVis
        void split_kinVis(const Array<OneD, NekDouble> total_kinVis);

        /// Splits innaray of total_kinVis into two components - implicit and explicit viscosity.
        /// Updates values of m_imp_kinVis and m_exp_kinVis
        /// Only for testing
        void split_kinVis_forTest(const Array<OneD, NekDouble> total_kinVis);

        /**
         * Set the physical fields based on a restart file, or a function
         * describing the initial condition given in the session.
         * @param  initialtime           Time at which to evaluate the function.
         * @param  dumpInitialConditions Write the initial condition to file?
         */
        void v_SetInitialConditions(NekDouble initialtime,
                                                    bool dumpInitialConditions,
                                                    const int domain);
        
    };

    typedef std::shared_ptr<VarVisSimple>
                VarVisSimpleSharedPtr;

} //end of namespace


#endif
