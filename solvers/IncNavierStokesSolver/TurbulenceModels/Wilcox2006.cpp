///////////////////////////////////////////////////////////////////////////////
//
// File Wilcox2006.cpp
//
// For more information, please see: http://www.nektar.info
//
// The MIT License
//
// Copyright (c) 2006 Division of Applied Mathematics, Brown University (USA),
// Department of Aeronautics, Imperial College London (UK), and Scientific
// Computing and Imaging Institute, University of Utah (USA).
//
// License for the specific language governing rights and limitations under
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//
// Description:
//
///////////////////////////////////////////////////////////////////////////////

#include <LibUtilities/BasicConst/NektarUnivConsts.hpp>
#include <IncNavierStokesSolver/TurbulenceModels/Wilcox2006.h>
# include <LibUtilities/BasicUtils/ArrayDebugger.h>

using namespace std;

namespace Nektar
{

    Wilcox2006::Wilcox2006()
    {}

    Wilcox2006::Wilcox2006(const LibUtilities::SessionReaderSharedPtr        pSession,
                           const Array<OneD, MultiRegions::ExpListSharedPtr> pFields,
                           const NekDouble p_const_kinVis,
                           const SolverUtils::AdvectionSharedPtr p_advObject)
    {
        ASSERTL0(false,
                 "This constructor should never run.");

    }

    void Wilcox2006::InitializeAllModelConstants(const LibUtilities::SessionReaderSharedPtr pSession)
    {
        NekDouble m_Sigma__def = 1.0/2.0;
        NekDouble m_Sigma_Star__def = 3.0/5.0;
        NekDouble m_Sigma_V__def = 1.0;
        NekDouble m_CD_const__def = 1.0/8.0;
        NekDouble m_Beta_Star__def = 9.0/100.0;
        NekDouble m_Alpha__def = 13.0/25.0;
        NekDouble m_Beta__def = 0.0708;
        NekDouble ZapnoutCoupling__def = 1.0;

        pSession->LoadParameter("Sigma", m_Sigma, m_Sigma__def);
        pSession->LoadParameter("Sigma_Star", m_Sigma_Star, m_Sigma_Star__def);
        pSession->LoadParameter("Sigma_V", m_Sigma_V, m_Sigma_V__def);
        pSession->LoadParameter("CD_const", m_CD_const, m_CD_const__def);
        pSession->LoadParameter("Beta_Star", m_Beta_Star, m_Beta_Star__def);
        pSession->LoadParameter("Alpha", m_Alpha, m_Alpha__def);
        pSession->LoadParameter("Beta", m_Beta, m_Beta__def);
        pSession->LoadParameter("ZapnoutCoupling", m_ZapnoutCoupling, ZapnoutCoupling__def);
    }

    void Wilcox2006::init_object(const LibUtilities::SessionReaderSharedPtr pSession,
                                 const Array<OneD, MultiRegions::ExpListSharedPtr> pFields,
                                 const NekDouble p_const_kinVis,
                                 const SolverUtils::AdvectionSharedPtr p_advObject,
                                 SolverUtils::EquationSystem *pEquationSystem)
    {
        // Set m_turKinEner to point to proper field of pFields
        for (int i = 0; i < pFields.size(); i++){
            if (boost::iequals(pSession->GetVariable(i), "k"))
            {
                m_turKinEnergy = pFields[i];
                m_turKinEner_index = i;
                break;
            }
            ASSERTL0(i != (pFields.size()-1),
                     "Need to set up turbulent kinetic energy field definition "
                     "= need variable \"k\".");
        }

        // Set m_turOmega to point to proper field of pFields
        for (int i = 0; i < pFields.size(); i++){
            if (boost::iequals(pSession->GetVariable(i), "omega"))
            {
                m_turOmega = pFields[i];
                m_turOmega_index = i;
                break;
            }
            ASSERTL0(i != (pFields.size()-1),
                     "Need to set up omega field definition "
                     "= need variable \"omega\".");
        }

        // Initialize arrays of phys points of k and omega
        CopyArray(m_turKinEnergy->GetPhys(), m_turKinEnergy_Phys);
        CopyArray(m_turOmega->GetPhys(), m_turOmega_Phys);
        m_const_kinVis = p_const_kinVis;
        m_advObject = p_advObject;

        // Initialize helm coeffs
        m_imp_HelmCoeffs_k = Array<OneD, NekDouble> (m_turKinEnergy_Phys.size());
        m_imp_HelmCoeffs_Omega = Array<OneD, NekDouble> (m_turOmega_Phys.size());
        m_exp_HelmCoeffs_k = Array<OneD, NekDouble> (m_turKinEnergy_Phys.size());
        m_exp_HelmCoeffs_Omega = Array<OneD, NekDouble> (m_turOmega_Phys.size());

        // Initialize explicit source terms for testing
        if (pSession->DefinesFunction("SourceTerms")) {
            m_sourceTerms = pEquationSystem->GetFunction("SourceTerms");
        } else {
            m_sourceTerms = nullptr;
        }

        // Initialize model constants
        InitializeAllModelConstants(pSession);
    }

    void Wilcox2006::setK(const Array<OneD, NekDouble> &k_phys)
    {
        CopyArray(k_phys, m_turKinEnergy_Phys);
    }

    void Wilcox2006::setOmega(const Array<OneD, NekDouble> &omega_phys)
    {
        CopyArray(omega_phys, m_turOmega_Phys);
    }

    void Wilcox2006::add_2times_SVGnorm(const Array<OneD, NekDouble> &du1_dx1, const Array<OneD, NekDouble> &du1_dx2, const Array<OneD, NekDouble> &du2_dx1, const Array<OneD, NekDouble> &du2_dx2, Array<OneD, NekDouble> &outarray)
    {
        int nPoints = outarray.size();
        Array<OneD, NekDouble> tempResult(nPoints);
        Vmath::Vmul(nPoints, du1_dx1, 1, du2_dx2, 1, tempResult, 1);
        Vmath::Smul(nPoints, (-4.0), tempResult, 1, tempResult, 1);

        Vmath::Vadd(nPoints, tempResult, 1, outarray, 1, outarray, 1);

        Vmath::Vadd(nPoints, du1_dx2, 1, du2_dx1, 1, tempResult, 1);

        Vmath::Vpow(nPoints, tempResult, 1, 2.0, tempResult, 1);
        Vmath::Vadd(nPoints, tempResult, 1, outarray, 1, outarray, 1);
    }

    void Wilcox2006::add_prodTerms(const Array<OneD, NekDouble> &du1_dx1,
                                   const Array<OneD, NekDouble> &du1_dx2,
                                   const Array<OneD, NekDouble> &du2_dx1,
                                   const Array<OneD, NekDouble> &du2_dx2,
                                   Array<OneD, NekDouble> &k_outarray,
                                   Array<OneD, NekDouble> &Omega_outarray)
    {

        int nPoints = k_outarray.size();
        Array<OneD, NekDouble> tempResult(nPoints);
        Array<OneD, NekDouble> k_prodTerm(nPoints);

        Vmath::Zero(nPoints, tempResult, 1);
        add_2times_SVGnorm(du1_dx1, du1_dx2, du2_dx1, du2_dx2, tempResult);

        Vmath::Vdiv(nPoints, m_turKinEnergy_Phys, 1, m_turOmega_Phys, 1, k_prodTerm, 1);
        Vmath::Vmul(nPoints, tempResult, 1, k_prodTerm, 1, k_prodTerm, 1);
        Vmath::Vadd(nPoints, k_prodTerm, 1, k_outarray, 1, k_outarray, 1);


        Vmath::Smul(nPoints, m_Alpha, tempResult, 1, tempResult, 1);
        Vmath::Vadd(nPoints, tempResult, 1, Omega_outarray, 1, Omega_outarray, 1);

    }
    

    void Wilcox2006::add_kProdTerm(const Array<OneD, NekDouble> &du1_dx1,
                                                       const Array<OneD, NekDouble> &du1_dx2,
                                                       const Array<OneD, NekDouble> &du2_dx1,
                                                       const Array<OneD, NekDouble> &du2_dx2,
                                                       Array<OneD, NekDouble> &outarray)
    {
        int nPoints = outarray.size();
        Array<OneD, NekDouble> tempResult(nPoints);

        Vmath::Vmul(nPoints, du1_dx1, 1, du2_dx2, 1, tempResult, 1);
        Vmath::Smul(nPoints, (-4.0), tempResult, 1, tempResult, 1);

        Vmath::Vadd(nPoints, tempResult, 1, outarray, 1, outarray, 1);

        Vmath::Vadd(nPoints, du1_dx2, 1, du2_dx1, 1, tempResult, 1);
        Vmath::Vpow(nPoints, tempResult, 1, 2.0, outarray, 1);

        Vmath::Vadd(nPoints, tempResult, 1, outarray, 1, outarray, 1);
    }

    void Wilcox2006::add_OmegaProdTerm(const Array<OneD, NekDouble> &du1_dx1,
                                       const Array<OneD, NekDouble> &du1_dx2,
                                       const Array<OneD, NekDouble> &du2_dx1,
                                       const Array<OneD, NekDouble> &du2_dx2,
                                       Array<OneD, NekDouble> &outarray)
    {
        int nPoints = outarray.size();
        Array<OneD, NekDouble> kProdTerm(nPoints);
        Array<OneD, NekDouble> tempResult(nPoints);

        add_kProdTerm(du1_dx1, du1_dx2, du2_dx1, du2_dx2, kProdTerm);
        Vmath::Vdiv(nPoints, m_turOmega_Phys, 1, m_turKinEnergy_Phys, 1, tempResult, 1);

        Vmath::Vmul(nPoints, kProdTerm, 1, tempResult, 1, tempResult, 1);
        Vmath::Smul(nPoints, m_Alpha, tempResult, 1, tempResult, 1);
        Vmath::Vadd(nPoints, tempResult, 1, outarray, 1, outarray, 1);

    }

    void Wilcox2006::add_OmegaReaction(Array<OneD, NekDouble> &outarray)
    {
        int nPoints = outarray.size();
        Array<OneD, NekDouble> tempResult(nPoints);

        Vmath::Vmul(nPoints, m_turOmega_Phys, 1, m_turOmega_Phys, 1, tempResult, 1);
        Vmath::Smul(nPoints, (-1.0)*m_Beta, tempResult, 1, tempResult, 1);
        Vmath::Vadd(nPoints, tempResult, 1, outarray, 1, outarray, 1);
    }

    void Wilcox2006::compute_turKinVis(const Array<OneD, NekDouble> &turKinEnergy,
                                       const Array<OneD, NekDouble> &turOmega,
                                       Array<OneD, NekDouble> &turKinVis)
    {
        int nPoints = turKinVis.size();
        Vmath::Vdiv(nPoints, turKinEnergy, 1, turOmega, 1, turKinVis, 1);
        Vmath::Smul(nPoints, m_Sigma_V,turKinVis,1,turKinVis,1);
    }

    void Wilcox2006::compute_totalKinVis(const Array<OneD, NekDouble> &turKinEnergy,
                                       const Array<OneD, NekDouble> &turOmega,
                                       Array<OneD, NekDouble> &totalKinVis)
    {
        int nPoints = totalKinVis.size();
        if (std::abs(m_ZapnoutCoupling) > NekConstants::kNekZeroTol) {
            compute_turKinVis(turKinEnergy, turOmega, totalKinVis);
            Vmath::Sadd(nPoints, m_const_kinVis, totalKinVis, 1, totalKinVis, 1);
        } else {
            Vmath::Zero(nPoints, totalKinVis, 1);
            Vmath::Sadd(nPoints, m_const_kinVis, totalKinVis, 1, totalKinVis, 1);
        }
    }

    void Wilcox2006::add_CrossDiffusivity(const Array<OneD, NekDouble> &dk_dx1,
                                              const Array<OneD, NekDouble> &dOmega_dx1,
                                              const Array<OneD, NekDouble> &dk_dx2,
                                              const Array<OneD, NekDouble> &dOmega_dx2,
                                              Array<OneD, NekDouble> &outarray)
    {
        int nPoints = outarray.size();
        Array<OneD, NekDouble> tempResult(nPoints);

        Vmath::Vvtvvtp(nPoints, dk_dx1, 1, dOmega_dx1, 1, dk_dx2, 1, dOmega_dx2, 1, tempResult, 1);

        auto iterOmega = m_turOmega_Phys.begin();
        for (auto iterResult = tempResult.begin(); iterResult != tempResult.end(); iterResult++, iterOmega++){
            if (*iterResult > 0){
                *iterResult =  (m_CD_const/(*iterOmega)) * (*iterResult);
            } else {
                *iterResult =  0;
            }
        }

        Vmath::Vadd(nPoints, tempResult, 1, outarray, 1, outarray, 1);
    }

    void Wilcox2006::add_DissipationRate(Array<OneD, NekDouble> &outarray)
    {
        int nPoints = outarray.size();
        Array<OneD, NekDouble> tempResult(nPoints);

        Vmath::Smul(nPoints, (-1.0)*m_Beta_Star, m_turKinEnergy_Phys, 1, tempResult, 1);
        Vmath::Vmul(nPoints, tempResult, 1, m_turOmega_Phys, 1, tempResult, 1);
        Vmath::Vadd(nPoints, tempResult, 1, outarray, 1, outarray, 1);
    }

    void Wilcox2006::split_HelmCoeffs_k(const Array<OneD, NekDouble> &total_HelmCoeffs_k)
    {
        int nPoints = m_imp_HelmCoeffs_k.size();
        // Use m_exp_kinVis as a storage for ratio between kinVis and imp_kinVis
        Vmath::Vdiv(nPoints, total_HelmCoeffs_k, 1, m_imp_HelmCoeffs_k, 1, m_exp_HelmCoeffs_k, 1);
        NekDouble max_ratio = Vmath::Vmax(nPoints, m_exp_HelmCoeffs_k, 1);
        NekDouble min_ratio = Vmath::Vmin(nPoints, m_exp_HelmCoeffs_k, 1);

        if ((max_ratio < (3.0/2.0)) && (min_ratio > (1.0/2.0))){
            // Implicit viscosity is OK, we need to calculate only explicit one
            Vmath::Vsub(nPoints, total_HelmCoeffs_k, 1,m_imp_HelmCoeffs_k, 1, m_exp_HelmCoeffs_k, 1);
        } else {
            // We need to update implicit viscosity to ensure the stability of the scheme
            Vmath::Vcopy(nPoints, total_HelmCoeffs_k, 1, m_imp_HelmCoeffs_k, 1);
            Vmath::Zero(nPoints, m_exp_HelmCoeffs_k, 1);
        }
    }

    void Wilcox2006::split_HelmCoeffs_Omega(const Array<OneD, NekDouble> &total_HelmCoeffs_Omega)
    {
        int nPoints = m_imp_HelmCoeffs_Omega.size();
        // Use m_exp_kinVis as a storage for ratio between kinVis and imp_kinVis
        Vmath::Vdiv(nPoints, total_HelmCoeffs_Omega, 1, m_imp_HelmCoeffs_Omega, 1, m_exp_HelmCoeffs_Omega, 1);
        NekDouble max_ratio = Vmath::Vmax(nPoints, m_exp_HelmCoeffs_Omega, 1);
        NekDouble min_ratio = Vmath::Vmin(nPoints, m_exp_HelmCoeffs_Omega, 1);

        if ((max_ratio < (3.0/2.0)) && (min_ratio > (1.0/2.0))){
            // Implicit viscosity is OK, we need to calculate only explicit one
            Vmath::Vsub(nPoints, total_HelmCoeffs_Omega, 1,m_imp_HelmCoeffs_Omega, 1, m_exp_HelmCoeffs_Omega, 1);
        } else {
            // We need to update implicit viscosity to ensure the stability of the scheme

            Vmath::Vcopy(nPoints, total_HelmCoeffs_Omega, 1, m_imp_HelmCoeffs_Omega, 1);
            Vmath::Zero(nPoints, m_exp_HelmCoeffs_Omega, 1);
        }
    }

    void Wilcox2006::compute_HelmCoeffs_k(Array<OneD, NekDouble> &total_HelmCoeffs_k,
                                          const Array<OneD, NekDouble> &turKinVis) const
    {
        int nPoints = total_HelmCoeffs_k.size();
        Vmath::Zero(nPoints, total_HelmCoeffs_k, 1);
        Vmath::Vadd(nPoints, turKinVis, 1, total_HelmCoeffs_k, 1, total_HelmCoeffs_k, 1);

        Vmath::Smul(nPoints, m_Sigma_Star, total_HelmCoeffs_k, 1, total_HelmCoeffs_k, 1);

        Vmath::Sadd(nPoints, m_const_kinVis, total_HelmCoeffs_k, 1, total_HelmCoeffs_k, 1);
    }

    void Wilcox2006::compute_HelmCoeffs_Omega(Array<OneD, NekDouble> &total_HelmCoeffs_Omega,
                                              const Array<OneD, NekDouble> &turKinVis)
    {
        int nPoints = total_HelmCoeffs_Omega.size();
        Vmath::Zero(nPoints, total_HelmCoeffs_Omega, 1);
        Vmath::Vadd(nPoints, turKinVis, 1, total_HelmCoeffs_Omega, 1, total_HelmCoeffs_Omega, 1);

        Vmath::Smul(nPoints, m_Sigma, total_HelmCoeffs_Omega, 1, total_HelmCoeffs_Omega, 1);

        Vmath::Sadd(nPoints, m_const_kinVis, total_HelmCoeffs_Omega, 1, total_HelmCoeffs_Omega, 1);
    }

    void Wilcox2006::compute_HelmCoeffs_Velocity(Array<OneD, NekDouble> &total_HelmCoeffs_Velocity,
                                                 const Array<OneD, NekDouble> &turKinVis)
    {
        int nPoints = total_HelmCoeffs_Velocity.size();
        Vmath::Zero(nPoints, total_HelmCoeffs_Velocity, 1);
        Vmath::Vadd(nPoints, turKinVis, 1, total_HelmCoeffs_Velocity, 1, total_HelmCoeffs_Velocity, 1);

        Vmath::Sadd(nPoints, m_const_kinVis, total_HelmCoeffs_Velocity, 1, total_HelmCoeffs_Velocity, 1);
    }

    void Wilcox2006::DoExplicitTerms(const Array<OneD, NekDouble> &k_inarray, const Array<OneD, NekDouble> &Omega_inarray,
                                     Array<OneD, NekDouble> &k_outarray, Array<OneD, NekDouble> &Omega_outarray,
                                     NekDouble time)
    {
        setK(k_inarray);
        setOmega(Omega_inarray);

        if (m_sourceTerms != nullptr){
            Array<OneD, NekDouble> k_sourceTerm(k_outarray.size());
            Array<OneD, NekDouble> omega_sourceTerm(Omega_outarray.size());

            m_sourceTerms->Evaluate("k", k_sourceTerm, time, 0);
            m_sourceTerms->Evaluate("omega", omega_sourceTerm, time, 0);

            int nPoints = k_outarray.size();
            Vmath::Vadd(nPoints, k_sourceTerm, 1, k_outarray, 1, k_outarray, 1);
            Vmath::Vadd(nPoints, omega_sourceTerm, 1, Omega_outarray, 1, Omega_outarray, 1);
        }

        // Tady už se do outarray jen přidává
        add_prodTerms(
                    m_advObject->convecFldsGrads[0],
                    m_advObject->convecFldsGrads[1],
                    m_advObject->convecFldsGrads[2],
                    m_advObject->convecFldsGrads[3],
                    k_outarray,
                    Omega_outarray
                    );

        add_DissipationRate(k_outarray);

        add_OmegaReaction(Omega_outarray);

        add_CrossDiffusivity(
                    m_advObject->convecFldsGrads[m_turKinEner_index*2],
                    m_advObject->convecFldsGrads[m_turOmega_index*2],
                    m_advObject->convecFldsGrads[m_turKinEner_index*2 + 1],
                    m_advObject->convecFldsGrads[m_turOmega_index*2 + 1],
                    Omega_outarray);

        int nPoints = k_inarray.size();
        Array<OneD, NekDouble> turKinVis(nPoints);
        Array<OneD, NekDouble> total_Coeffs(nPoints);

        compute_turKinVis(k_inarray, Omega_inarray, turKinVis);

        compute_HelmCoeffs_k(total_Coeffs, turKinVis);
        split_HelmCoeffs_k(total_Coeffs);

        compute_HelmCoeffs_Omega(total_Coeffs, turKinVis);
        split_HelmCoeffs_Omega(total_Coeffs);

        add_ExplicitDiffusion_k(m_exp_HelmCoeffs_k, k_outarray);
        add_ExplicitDiffusion_Omega(m_exp_HelmCoeffs_Omega, Omega_outarray);
    }

    void Wilcox2006::DoImplicitTerms(const Array<OneD, NekDouble> &k_inarray, const Array<OneD, NekDouble> &Omega_inarray,
                                     Array<OneD, NekDouble> &k_outarray, Array<OneD, NekDouble> &Omega_outarray,
                                     const NekDouble aii_Dt)
    {
        int nPoints = m_turKinEnergy->GetTotPoints();
        Array<OneD, NekDouble> k_Forcing(nPoints);
        Array<OneD, NekDouble> Omega_Forcing(nPoints);

        Vmath::Smul(nPoints, (-1.0)/aii_Dt, k_inarray, 1, k_Forcing, 1);
        Vmath::Smul(nPoints, (-1.0)/aii_Dt, Omega_inarray, 1, Omega_Forcing, 1);

        Array<OneD, NekDouble> m_imp_HelmCoeffs_k_Copy;
        Array<OneD, NekDouble> m_imp_HelmCoeffs_Omega_Copy;
        CopyArray(m_imp_HelmCoeffs_k, m_imp_HelmCoeffs_k_Copy);
        CopyArray(m_imp_HelmCoeffs_Omega, m_imp_HelmCoeffs_Omega_Copy);

        StdRegions::VarCoeffMap varCoeffMap_k =
        {{StdRegions::eVarCoeffD00, m_imp_HelmCoeffs_k_Copy},
         {StdRegions::eVarCoeffD11, m_imp_HelmCoeffs_k_Copy}};

        StdRegions::VarCoeffMap varCoeffMap_Omega =
        {{StdRegions::eVarCoeffD00, m_imp_HelmCoeffs_Omega_Copy},
         {StdRegions::eVarCoeffD11, m_imp_HelmCoeffs_Omega_Copy}};


        MultiRegions::VarFactorsMap varFactorsMap =
            MultiRegions::NullVarFactorsMap;
        StdRegions::ConstFactorMap factors;
        factors[StdRegions::eFactorLambda] = 1.0/aii_Dt;

        m_turKinEnergy->HelmSolve(k_Forcing, m_turKinEnergy->UpdateCoeffs(), factors, varCoeffMap_k,
                               varFactorsMap);
        m_turKinEnergy->BwdTrans(m_turKinEnergy->GetCoeffs(),k_outarray);

        m_turOmega->HelmSolve(Omega_Forcing, m_turOmega->UpdateCoeffs(), factors, varCoeffMap_Omega,
                               varFactorsMap);
        m_turOmega->BwdTrans(m_turOmega->GetCoeffs(), Omega_outarray);
    }

    void Wilcox2006::add_ExplicitDiffusion_k(const Array<OneD, NekDouble> &exp_HelmCoeffs,
                                             Array<OneD, NekDouble> &outarray)
    {
        int nPointsTot = m_turKinEnergy->GetNpoints();
        Array<OneD, NekDouble> term_1 = Array<OneD, NekDouble> (nPointsTot);
        Array<OneD, NekDouble> term_2 = Array<OneD, NekDouble> (nPointsTot);


        // Computation of exp_HelmCoeff * grad(k)
        Vmath::Vmul(nPointsTot, exp_HelmCoeffs, 1, m_advObject->convecFldsGrads[m_turKinEner_index*2], 1, term_1,1);
        Vmath::Vmul(nPointsTot, exp_HelmCoeffs, 1, m_advObject->convecFldsGrads[m_turKinEner_index*2 + 1], 1, term_2,1);

        // Divergence
        m_turKinEnergy->PhysDeriv(0,term_1, term_1);
        m_turKinEnergy->PhysDeriv(1, term_2, term_2);

        Vmath::Vadd(nPointsTot, term_1, 1, term_2, 1, term_1, 1);

        // Add to outarray
        Vmath::Vadd(nPointsTot, term_1, 1, outarray, 1, outarray, 1);
    }

    void Wilcox2006::add_ExplicitDiffusion_Omega(const Array<OneD, NekDouble> &exp_HelmCoeffs,
                                             Array<OneD, NekDouble> &outarray)
    {
        int nPointsTot = m_turOmega->GetNpoints();
        Array<OneD, NekDouble> term_1 = Array<OneD, NekDouble> (nPointsTot);
        Array<OneD, NekDouble> term_2 = Array<OneD, NekDouble> (nPointsTot);


        // Computation of exp_HelmCoeff * grad(k)
        Vmath::Vmul(nPointsTot, exp_HelmCoeffs, 1, m_advObject->convecFldsGrads[m_turOmega_index*2], 1, term_1,1);
        Vmath::Vmul(nPointsTot, exp_HelmCoeffs, 1, m_advObject->convecFldsGrads[m_turOmega_index*2 + 1], 1, term_2,1);

        // Divergence
        m_turOmega->PhysDeriv(0,term_1, term_1);
        m_turOmega->PhysDeriv(1, term_2, term_2);

        Vmath::Vadd(nPointsTot, term_1, 1, term_2, 1, term_1, 1);

        // Add to outarray
        Vmath::Vadd(nPointsTot, term_1, 1, outarray, 1, outarray, 1);
    }

    void Wilcox2006::wallFunction(int bnd_id,
                                  const MultiRegions::ExpListSharedPtr field,
                                  Array<OneD, NekDouble> dist)
    {
       // ASSERTL0(field->GetNpoints() == dist.size(), "Wall function cannot be computed. Arguments sizes mismatch.");
        int nPoints = field->GetNpoints();
        Array<OneD, NekDouble> x(nPoints);
        Array<OneD, NekDouble> y(nPoints);
        Array<OneD, NekDouble> z(nPoints);
        field->GetCoords(x, y, z);

        //MultiRegions::ExpListSharedPtr bnd_field;
        //field->GetBndElmtExpansion(bnd_id, bnd_field, false);

        int bnd_nPoints = (field->GetBndCondExpansions())[bnd_id]->GetNpoints();
        //int bnd_nPoints = bnd_field->GetNpoints();
        Array<OneD, NekDouble> bnd_x(bnd_nPoints);
        Array<OneD, NekDouble> bnd_y(bnd_nPoints);
        Array<OneD, NekDouble> bnd_z(bnd_nPoints);
        //bnd_field->GetCoords(bnd_x, bnd_y, bnd_z);
        (field->GetBndCondExpansions())[bnd_id]->GetCoords(bnd_x, bnd_y, bnd_z);

        for (int i = 0; i < nPoints; ++i) {
            dist[i]=getMinDistToBnd(bnd_x,bnd_y,bnd_z, x[i], y[i], z[i]);
        }
    }

    NekDouble Wilcox2006::getMinDistToBnd(const Array<OneD, NekDouble> bnd_points_x_coor,
                                          const Array<OneD, NekDouble> bnd_points_y_coor,
                                          const Array<OneD, NekDouble> bnd_points_z_coor,
                                          const NekDouble point_x,
                                          const NekDouble point_y,
                                          const NekDouble point_z)
    {
        ASSERTL0(bnd_points_x_coor.size() == bnd_points_y_coor.size() &&
                 bnd_points_x_coor.size() == bnd_points_z_coor.size(),
                 "Distance to boundary cannot be computed. Arguments sizes mismatch.");

        NekDouble min_dist = DBL_MAX;
        NekDouble dist;

        int nPoints = bnd_points_x_coor.size();
        for (int i = 0; i < nPoints; ++i) {
            dist = sqrt((bnd_points_x_coor[i] - point_x) * (bnd_points_x_coor[i] - point_x) +
                        (bnd_points_y_coor[i] - point_y) * (bnd_points_y_coor[i] - point_y) +
                        (bnd_points_z_coor[i] - point_z) * (bnd_points_z_coor[i] - point_z));
            min_dist = std::min(min_dist, dist);
        }
        return min_dist;
    }

    NekDouble Wilcox2006::getMinDistToComposite(const SpatialDomains::CompositeSharedPtr composite,
                                                    const NekDouble point_x, const NekDouble point_y, const NekDouble point_z)
    {
        NekDouble min_dist = DBL_MAX;
        NekDouble elm_x, elm_y, elm_z;
        NekDouble dist;

        for (auto &&elm : composite->m_geomVec){
            // Each element is (maybe curved) line segment with two end points
            for (int i = 0; i < 2; ++i) {
                elm->GetVertex(i)->GetCoords(elm_x, elm_y, elm_z);
                dist = sqrt((elm_x - point_x) * (elm_x - point_x) +
                            (elm_y - point_y) * (elm_y - point_y) +
                            (elm_z - point_z) * (elm_z - point_z));
                min_dist = std::min(min_dist, dist);
            }
        }
        return min_dist;
    }

    NekDouble Wilcox2006::getMinDistanceToComposite(SpatialDomains::CompositeSharedPtr composite, SpatialDomains::PointGeomSharedPtr point)
    {
        NekDouble min_dist = point->dist(*(composite->m_geomVec[0]->GetVertex(0)));

        for (auto &&x : composite->m_geomVec){
            min_dist = std::min(min_dist, point->dist(*(x->GetVertex(0))));
            min_dist = std::min(min_dist, point->dist(*(x->GetVertex(1))));
        }
        return min_dist;
    }
} //end of namespace

