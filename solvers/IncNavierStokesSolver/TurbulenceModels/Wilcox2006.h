///////////////////////////////////////////////////////////////////////////////
//
// File Wilcox2006.h
//
// For more information, please see: http://www.nektar.info
//
// The MIT License
//
// Copyright (c) 2006 Division of Applied Mathematics, Brown University (USA),
// Department of Aeronautics, Imperial College London (UK), and Scientific
// Computing and Imaging Institute, University of Utah (USA).
//
// License for the specific language governing rights and limitations under
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//
// Description: 
//
///////////////////////////////////////////////////////////////////////////////

#ifndef NEKTAR_SOLVERS_WILCOX2006_H
#define NEKTAR_SOLVERS_WILCOX2006_H

#include <string>
#include <functional>

#include <LibUtilities/BasicUtils/NekFactory.hpp>
#include <LibUtilities/BasicUtils/SharedArray.hpp>
#include <MultiRegions/ExpList.h>
#include <SolverUtils/SolverUtilsDeclspec.h>
#include <SolverUtils/Advection/Advection.h>
#include <SolverUtils/Core/SessionFunction.h>
#include <SolverUtils/EquationSystem.h>


namespace Nektar
{     

class Wilcox2006

{
public:
    Wilcox2006();

    Wilcox2006(const LibUtilities::SessionReaderSharedPtr pSession,
               const Array<OneD, MultiRegions::ExpListSharedPtr> pFields,
               const NekDouble p_const_kinVis,
               const SolverUtils::AdvectionSharedPtr p_advObject);

    void init_object(const LibUtilities::SessionReaderSharedPtr pSession,
                     const Array<OneD, MultiRegions::ExpListSharedPtr> pFields,
                     const NekDouble p_const_kinVis,
                     const SolverUtils::AdvectionSharedPtr p_advObject,
                     SolverUtils::EquationSystem *pEquationSystem);

    /// This function enable user to specify all constants in Wilcox model
    void InitializeAllModelConstants(const LibUtilities::SessionReaderSharedPtr pSession);

    /// Set values of turbulent kinetic energy k. Should be used during explicit part of RHs.
    ///
    /// Only for purposes of object Wilcox2006
    void setK(const Array<OneD, NekDouble> &k_phys);

    /// Set values of omega. Should be used during explicit part of RHs.
    ///
    /// Only for purposes of object Wilcox2006
    void setOmega(const Array<OneD, NekDouble> &omega_phys);

    /// Computes squared norm of symmetric velocity gradient times 2.0  ___in 2D___
    /// 2*S_ij*S_ij = -4*(dU_1/dx_1)*(dU_2/dx_2)+(dU_1/dx_2 + dU_2/dx_1)^2
    ///
    /// Useful for k and Omega production terms.
    /// Adds the computed value to outarray.
    /// outarray += 2*S_ij*S_ij
    /// 2*S_ij*S_ij = -4*(dU_1/dx_1)*(dU_2/dx_2) + (dU_1/dx_2 + dU_2/dx_1)^2
    void add_2times_SVGnorm(const Array<OneD, NekDouble> &du1_dx1,
                                           const Array<OneD, NekDouble> &du1_dx2,
                                           const Array<OneD, NekDouble> &du2_dx1,
                                           const Array<OneD, NekDouble> &du2_dx2,
                                           Array<OneD, NekDouble> &outarray);

    /// Computes both production terms for k and for Omega
    /// k_outarray = (k/Omega)*2*S_ij*S_ij = (k/Omega)*[-4*(dU_1/dx_1)*(dU_2/dx_2)+(dU_1/dx_2 + dU_2/dx_1)^2]
    /// Omega_outarray = Alpha*2*S_ij*S_ij
    ///
    /// Adds the computed values to outarrays.
    void add_prodTerms(const Array<OneD, NekDouble> &du1_dx1,
                                           const Array<OneD, NekDouble> &du1_dx2,
                                           const Array<OneD, NekDouble> &du2_dx1,
                                           const Array<OneD, NekDouble> &du2_dx2,
                                           Array<OneD, NekDouble> &k_outarray,
                                           Array<OneD, NekDouble> &Omega_outarray);

    /// Computes production term for turbulent kinetic energy _k_ in 2D.
    ///
    /// Adds the computed value to outarray.
    /// Alpha is constant.
    /// outarray += 2*S_ij*S_ij
    /// 2*S_ij*S_ij = -4*(dU_1/dx_1)*(dU_2/dx_2) + (dU_1/dx_2 + dU_2/dx_1)^2
    void add_kProdTerm(const Array<OneD, NekDouble> &du1_dx1,
                                           const Array<OneD, NekDouble> &du1_dx2,
                                           const Array<OneD, NekDouble> &du2_dx1,
                                           const Array<OneD, NekDouble> &du2_dx2,
                                           Array<OneD, NekDouble> &outarray);

    /// Computes production term for omega in 2D.
    ///
    /// That is squared norm of symmetric velocity gradient times 2.0*alfa*(omega/k)
    /// Adds the computed value to outarray.
    /// Alpha is constant.
    /// outarray += 2*S_ij*S_ij*Alpha*(omega/k)
    /// 2*S_ij*S_ij = -4*(dU_1/dx_1)*(dU_2/dx_2)+(dU_1/dx_2 + dU_2/dx_1)^2
    void add_OmegaProdTerm(const Array<OneD, NekDouble> &du1_dx1,
                                           const Array<OneD, NekDouble> &du1_dx2,
                                           const Array<OneD, NekDouble> &du2_dx1,
                                           const Array<OneD, NekDouble> &du2_dx2,
                                           Array<OneD, NekDouble> &outarray);

    /// Computes reaction term for omega in 2D.
    ///
    /// That is squared omega times Beta (constant).
    /// Adds the computed value to outarray.
    /// outarray += (-Beta) * Omega^2
    void add_OmegaReaction(Array<OneD, NekDouble> &outarray);


    /// Computes phys values of turbulent kinematic viscosity WITHOUT STRESS DELIMITER
    /// Overrides previous values in turKinVis.
    ///
    /// turKinVis = k/Omega = turKinEnergy/Omega
    void compute_turKinVis(const Array<OneD, NekDouble> &turKinEnergy,
                                       const Array<OneD, NekDouble> &turOmega,
                                       Array<OneD, NekDouble> &turKinVis);

    /// Computes phys values of total kinematic viscosity WITHOUT STRESS DELIMITER
    /// Overrides previous values in totalKinVis.
    ///
    /// totalKinVis = (k/Omega) + const_kinVis = (turKinEnergy/Omega) + const_kinVis
    void compute_totalKinVis(const Array<OneD, NekDouble> &turKinEnergy,
                                       const Array<OneD, NekDouble> &turOmega,
                                       Array<OneD, NekDouble> &totalKinVis);

    /// Computes cross diffusivity in 2D
    ///
    /// Adds the computed value to outarray.
    /// outarray += CrossDif
    /// CrossDif = (CD_const/Omega)*[(dk/dx_1)*(dOmega/dx_1) + (dk/dx_2)*(dOmega/dx_2)]
    void add_CrossDiffusivity(const Array<OneD, NekDouble> &dk_dx1,
                                    const Array<OneD, NekDouble> &dOmega_dx1,
                                    const Array<OneD, NekDouble> &dk_dx2,
                                    const Array<OneD, NekDouble> &dOmega_dx2,
                                    Array<OneD, NekDouble> &outarray);

    /// Computes dissipation rate in 2D
    /// Adds the computed value to outarray.
    /// outarray += DisRate
    /// DisRate = (-Beta_Star) * k * Omega
    void add_DissipationRate(Array<OneD, NekDouble> &outarray);

    void split_HelmCoeffs_Omega(const Array<OneD, NekDouble> &total_HelmCoeffs_Omega);
    void split_HelmCoeffs_k(const Array<OneD, NekDouble> &total_HelmCoeffs_k);

    void compute_HelmCoeffs_k(Array<OneD, NekDouble> &total_HelmCoeffs_k, const Array<OneD, NekDouble> &turKinVis) const;
    void compute_HelmCoeffs_Omega(Array<OneD, NekDouble> &total_HelmCoeffs_Omega,const Array<OneD, NekDouble> &turKinVis);
    void compute_HelmCoeffs_Velocity(Array<OneD, NekDouble> &total_HelmCoeffs_Velocity, const Array<OneD, NekDouble> &turKinVis);

    void DoExplicitTerms(const Array<OneD, NekDouble> &k_inarray, const Array<OneD, NekDouble> &Omega_inarray,
                         Array<OneD, NekDouble> &k_outarray, Array<OneD, NekDouble> &Omega_outarray, NekDouble time);

    void DoImplicitTerms(const Array<OneD, NekDouble> &k_inarray, const Array<OneD, NekDouble> &Omega_inarray,
                                     Array<OneD, NekDouble> &k_outarray, Array<OneD, NekDouble> &Omega_outarray,
                                     const NekDouble aii_Dt);

    void add_ExplicitDiffusion_k(const Array<OneD, NekDouble> &exp_HelmCoeffs,
                               Array<OneD, NekDouble> &outarray);

    void add_ExplicitDiffusion_Omega(const Array<OneD, NekDouble> &exp_HelmCoeffs,
                                             Array<OneD, NekDouble> &outarray);

    void wallFunction(int bnd_id, const MultiRegions::ExpListSharedPtr field, Array<OneD, NekDouble> dist);

    NekDouble getMinDistToBnd(const Array<OneD, NekDouble> bnd_points_x_coor,
                                          const Array<OneD, NekDouble> bnd_points_y_coor,
                                          const Array<OneD, NekDouble> bnd_points_z_coor,
                                          const NekDouble point_x,
                                          const NekDouble point_y,
                                          const NekDouble point_z);

    NekDouble getMinDistToComposite(const SpatialDomains::CompositeSharedPtr composite,
                                                    const NekDouble point_x, const NekDouble point_y, const NekDouble point_z);


    NekDouble getMinDistanceToComposite(SpatialDomains::CompositeSharedPtr mesh_graph, SpatialDomains::PointGeomSharedPtr point);

    /// Pointer to expansion containing kinetic turbulent energy k
    MultiRegions::ExpListSharedPtr m_turKinEnergy;

    /// Pointer to expansion containing specificit rate of dissipation omega
    MultiRegions::ExpListSharedPtr m_turOmega;

    /// which components of m_fields contains kinetic energy k
    int m_turKinEner_index;

    /// which components of m_fields contains specific dissipation rate omega
    int m_turOmega_index;

private:

    /// Pointer to advection object containing stored gradients
    SolverUtils::AdvectionSharedPtr m_advObject;

    /// Actual values of k and Omega
    /// Values are from last explicit RHs
    Array<OneD, NekDouble> m_turKinEnergy_Phys;
    Array<OneD, NekDouble> m_turOmega_Phys;

    /// Actual values of coeffs (used in HelmSolve) for k and omega
    /// kinVis_for_k = const_kinVis + Sigma_Star * (k/Omega)
    /// kinVis_for_Omega = const_kinVis + Sigma * (k/Omega)
    /// Coeffs are then divided into explicit and implicit part
    Array<OneD, NekDouble> m_imp_HelmCoeffs_k;
    Array<OneD, NekDouble> m_exp_HelmCoeffs_k;
    Array<OneD, NekDouble> m_imp_HelmCoeffs_Omega;
    Array<OneD, NekDouble> m_exp_HelmCoeffs_Omega;

    /// Source terms
    SolverUtils::SessionFunctionSharedPtr m_sourceTerms;

    /// Values of different constants
    /// They are modified only during construction of object Wilcox2006
    NekDouble m_Sigma;
    NekDouble m_Sigma_Star;
    NekDouble m_Sigma_V;
    NekDouble m_CD_const;
    NekDouble m_Beta_Star;
    NekDouble m_Alpha;
    NekDouble m_Test;
    NekDouble m_Beta;
    NekDouble m_ZapnoutCoupling;

    /// value of constant kinematic viscosity
    NekDouble m_const_kinVis;

};

    
} //end of namespace

#endif
