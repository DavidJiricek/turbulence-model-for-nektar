Turbulence model for Nektar++
=============================
This repository contains implementation of **k-omega turbulence model**,
developed upon software element framework **Nektar++**. It can be used for simulations
of incompressible turbulent flows and flows with variable viscosity. 
It is one of the first turbulence models implemented using spectral/hp element method. 

Source code of the model is embedded within Nektar++ framework,
it is a part of incompressible Navier-Stokes solver. Most important classes
are _VarVisSimple_ and _Wilcox2006_.

The most of the code in the repository is of Nektar++ itself,
which contains more solvers and also libraries necessary to run the turbulence model.

The model implementation was created as a master thesis. The thesis serves also 
as a documentation and it is located in the folder _TurbulenceModel_ 
together with additional examples.

Original model equations can be found in the book Turbulence Modeling for CFD [1]
where they are refered to as _Wilcox (2006)_ model.

Installation
------------

The model is installed together with Nektar++ incompressible 
Navier-Stokes solver. No additional steps are required. See section
_Installation of Nektar++_.

Usage
-----

General usage of Nektar++ framework is described in User Guide.

Specific settings for the turbulence model are explained in section 4.5
_Setting Nektar++ input file_ of the master thesis (in the folder _TurbulenceModel_).


Nektar++
========
Nektar++ is an open-source software framework designed to support the
development of high-performance scalable solvers for partial differential
equations (PDEs) using the spectral/hp element method.

This package consists of a set of libraries (the framework) and a number of
pre-written PDE solvers for a selection of application domains.

The software and User Guide is available for download from
<http://www.nektar.info/>.


User Guide
----------
Detailed information on compiling, installing and using the software is
available in the User Guide. This document is available as a pre-compiled PDF
from the downloads section of the project website.


Tutorials
---------
A number of tutorials are available, designed to walk the user through the
basics of spectral/hp element methods, through the use of individual solvers and
performing specific types of calculations.

The tutorials are available from <http://doc.nektar.info/tutorials/latest>.


Pre-requisites
--------------
Nektar++ requires the following software to be installed on the users system:

- CMake
- BLAS/LAPACK

Additional software is also required. This can either be installed system-wide
or it can be downloaded and compiled automatically during the build process.

For more detailed information, please see the User Guide.


Compilation
-----------
On most UNIX-based systems a default compilation can be performed using the
following commands from the top-level of the source tree:

    mkdir build
    cd build
    cmake ..
    make

To alter the build configuration (for example, to enable parallel execution
support) we recommend using the `ccmake` command instead of `cmake`. 

For more detailed operating-system specific instructions, please see the
User Guide.


Installation of Nektar++
------------------------
The default installation location is in a `dist` subdirectory of the `build`
directory. This can be changed by setting the `CMAKE_INSTALL_PREFIX` option
using `ccmake`. To install the compiled libraries, solvers and header files, on
UNIX-based systems run:

    make install


[1]: D. Wilcox. Turbulence modeling for CFD. DCW Industries, La Canada, California,
2006. ISBN 9781928729082
