///////////////////////////////////////////////////////////////////////////////
//
// File ArrayDebugger.cpp
//
// For more information, please see: http://www.nektar.info
//
// The MIT License
//
// Copyright (c) 2006 Scientific Computing and Imaging Institute,
// University of Utah (USA) and Department of Aeronautics, Imperial
// College London (UK).
//
// License for the specific language governing rights and limitations under
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//
// Description: Introduces global variable for debugging arrays.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef LIBUTILITIES_BASICUTILS_ARRAYDEBUGGER_H
#define LIBUTILITIES_BASICUTILS_ARRAYDEBUGGER_H

#include <ostream>
#include <vector>
#include <string>
#include <LibUtilities/BasicUtils/SharedArray.hpp>
#include <LibUtilities/BasicUtils/ErrorUtil.hpp>

namespace Nektar
{
    class ArrayDebugger {
    public:
        // Singleton desing
        static ArrayDebugger& getInstance()
        {
            static ArrayDebugger instance; // Guaranteed to be destroyed.
                                  // Instantiated on first use.
            return instance;
        }
        ArrayDebugger(ArrayDebugger const&) = delete;
        void operator=(ArrayDebugger const&)  = delete;
        // ---------------------------

        void readAnotherArray(const Array<OneD, const NekDouble>& array, const std::string& name_of_array){
            StoredArray newArray(array, name_of_array);
            for(auto x : storage){
                newArray.InsertEquality(x.name, newArray == x);
            }
            storage.push_back(newArray);

        }

        void printAll(){
            for(auto&& x : storage){
                x.printOnlyValues();
            }
        }

        void printOnlyValues(int id){
            storage[id].printOnlyValues();
        }

        void clear(){
            storage.clear();
        }

        void debug(){
                ++dummy;
        }

    private:
        ArrayDebugger():dummy(10) {} // can be instatinatied only through getInstance

        class StoredArray {
        public:
            std::string name;
            std::vector<NekDouble> values;
            std::vector<std::string> equalities;
            bool containsNaN;

            StoredArray(const Array<OneD, const NekDouble>& array, const std::string& name_of_array)
                : name(name_of_array), containsNaN(false)
            {
                for (auto iter = array.begin(); iter != array.end(); ++iter) {
                    values.push_back(*iter);
                    if (std::isnan(*iter) || std::isinf(*iter)) containsNaN = true;
                }
                equalities.push_back("----- " + name_of_array + " -----");
            }

            void InsertEquality(std::string another_name, bool isEqual){
                if (isEqual){
                    equalities.push_back(another_name + "   OK");
                } else {
                    equalities.push_back(another_name + "   FALSE");
                }
            }

            bool operator== (const StoredArray &anotherArray)
            {
                return (this->values == anotherArray.values);
            }

            void printOnlyValues(){
                std::cout << std::endl;
                std::cout << "***___" << name << "___***" << std::endl;
                for (auto&& x : values){
                    std::cout << std::to_string(x) << std::endl;
                }
            }
        };

        int dummy;
        std::vector<StoredArray> storage;
    };
}

#endif
